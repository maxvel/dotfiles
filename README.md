# Configurations (dotfiles)
This repository holds all the configuration files that I use to manage my day-to-day workflow. Creating config files from scratch (if they are lost) can be a pain in the butt. This is a safe place that would hold them for good :)

### Note to self
1. Make sure to populate .env files before running `docker-compose up -d` and you should be fine. 